# Covid-19 Tracker

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

- [Covid-19 Tracker](#covid-19-tracker)
  - [Table of Contents](#table-of-contents)
  - [Background](#background)
  - [Assumptions](#assumptions)
  - [Usage](#usage)
  - [Maintainers](#maintainers)
  - [Contribute](#contribute)
  - [License](#license)

## Background

This project is based on the data provded by The New York Times, located [here](https://github.com/nytimes/covid-19-data). It uses Docker to compose [Metabase](https://www.metabase.com/) on top of [PostgreSQL](https://www.postgresql.org/docs/12/intro-whatis.html), with the provided Docker file automatically loading the NYT data while building the image.

## Assumptions

This project relies on [Docker](https://www.docker.com/), therefore you must have it installed to use this project. All development done on MacOS, not validated with Windows.

## Usage

Use the follow command to run the application:
```console
$ make run
```

If you'd like to rebuild the images and then run:
```console
$ make build
```

After the project is running, navigate to [here](http://localhost:3001/) to use the Metabase UI. If required, follow the prompts to create a new user. Nothing is validated, so feel fee to use fake info.

The next step will ask you for a database connection. Select 'PostgreSQL' and enter the following connection info (all is found in the docker-compose file):

| Option            | Value          |
| ----------------- | -------------- |
| Name              | Your choice    |
| Host              | covid-postgres |
| Port              | Leave blank    |
| Database Name     | covid-db       |
| Database username | app            |
| Database password | abc123         |

## Maintainers

- @Benthescott

## Contribute

Submit issues and PRs.

## License

© Benjamin Cline
