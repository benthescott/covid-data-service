# Python parameters
PROJECTNAME="Covid-19 Cases"
# ENTRYFILE=""
MAKEFLAGS += --silent

# Run the application with Docker
run:
	docker-compose up

## build: Build and runs all containers
build:
	make clean
	docker-compose up --build --force-recreate

clean:
	rm -rf covid-db-data
	rm -rf metabase-data

.PHONY: help
all: help
help: Makefile
	@echo
	@echo "Choose a command to run in "$(PROJECTNAME)":"
	@echo
	sed -n 's/^##//p' $< | column -t -s ':' | sed -e 's/^/ /'
	@echo